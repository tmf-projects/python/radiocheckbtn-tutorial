from tkinter import *
import random

# это уже от меня, список номеров для 4-ой кнопки
random_number_list = ['597(4403)026-98-27', '0(92)092-20-24', '19(979)829-85-57', 
'85(873)940-17-03', '6(015)096-38-48', '22(86)691-53-16', '2(29)210-12-97', '82(256)325-92-74', 
'81(118)712-82-48', '0(349)142-39-15', '4(41)860-97-75', '08(200)524-60-39', '97(3015)017-98-44', 
'43(7496)106-24-57', '34(4621)385-22-06', '454(5087)495-07-94', '6(8805)915-21-10', 
'74(253)779-28-71', '9(2872)925-44-76', '34(8751)396-57-47', '325(424)307-52-30', 
'3(730)582-73-88', '7(22)651-59-12', '46(590)210-89-36', '1(396)231-75-77', '921(875)180-27-05', 
'37(6751)837-40-41', '7(551)429-55-75', '0(8955)653-02-70', '904(60)857-71-25', '355(40)873-27-59', 
'299(2587)341-47-62', '2(3781)648-40-82', '7(09)861-11-26', '989(62)544-29-36', '182(0287)592-61-85', 
'7(347)926-11-08', '1(3075)554-47-16', '67(118)282-90-30', '415(1320)844-55-12']


def btn1_number():
    label['text'] = '9(8974)763-40-29'
def btn2_number():
    label['text'] = '544(9725)404-14-72'
def btn3_number():
    label['text'] = '70(458)226-25-62'
def btn4_number():
    label['text'] = random_number_list[random.randint(0, len(random_number_list) - 1)]


root = Tk()

f1 = Frame()
f1.pack(side=LEFT)

btn1 = Radiobutton(f1, text='Вася', width=10, height=5, indicatoron=0, command=btn1_number)
btn2 = Radiobutton(f1, text='Петя', width=10, height=5, indicatoron=0, command=btn2_number)
btn3 = Radiobutton(f1, text='Маша', width=10, height=5, indicatoron=0, command=btn3_number)
btn4 = Radiobutton(f1, text='Случ.\nномер', width=10, height=5, indicatoron=0, command=btn4_number)

btn1.pack(anchor=W)
btn2.pack(anchor=W)
btn3.pack(anchor=W)
btn4.pack(anchor=W)

label = Label(text='Нажмите на имя,\nчтобы увидеть номер', width=30, height=15)

label.pack(side=RIGHT)

root.title('RadioCheckBtnTutorial')
root.resizable(0,0)
root.mainloop()